﻿using UnityEngine;

public class DestroyDamage : MonoBehaviour, ITakeDamage
{
    public void TakeDamage(Targetable targetable, int damage)
    {
        Destroy(targetable.gameObject);
    }
}
