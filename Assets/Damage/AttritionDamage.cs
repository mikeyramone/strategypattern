﻿using UnityEngine;

public class AttritionDamage : MonoBehaviour, ITakeDamage
{
    public void TakeDamage(Targetable targetable, int damage)
    {
        targetable.health -= damage;
        if (targetable.health < 0)
            Destroy(targetable.gameObject);
    }
}
