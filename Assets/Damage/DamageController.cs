﻿using UnityEngine;

public class DamageController : MonoBehaviour
{
    protected ITakeDamage iTakeDamage;
    protected Targetable targetable;

    private void Start()
    {
        GetTargetable();
    }

    public void InitAttritionDamage()
    {
        AttritionDamage attritionDamage = gameObject.AddComponent<AttritionDamage>();
        iTakeDamage = attritionDamage;
    }

    public void InitDestroyDamage()
    {
        DestroyDamage destroyDamage = gameObject.AddComponent<DestroyDamage>();
        iTakeDamage = destroyDamage;
    }

    private void GetTargetable()
    {
        targetable = gameObject.GetComponent<Targetable>();
        if (targetable == null)
        { Destroy(this);}
    }

    public void TakesDamage(int damage)
    {
        iTakeDamage.TakeDamage(targetable, damage);
    }
}
