﻿public interface ITakeDamage
{
    void TakeDamage(Targetable targetable, int damage);
}
