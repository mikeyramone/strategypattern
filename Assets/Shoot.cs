﻿using UnityEngine;

public class Shoot : MonoBehaviour
{   
    // Update is called once per frame
    void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {                    
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hitInfo;

            if (Physics.Raycast(ray, out hitInfo))
            {
                DamageController damageController =  hitInfo.collider.GetComponent<DamageController>();
                if (damageController != null)
                {
                    damageController.TakesDamage(1); 
                }
            }         
        }
	}
}
