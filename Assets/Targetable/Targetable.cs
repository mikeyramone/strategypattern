﻿using UnityEngine;

public abstract class Targetable : MonoBehaviour
{
    protected DamageController damageController;

    [SerializeField]
    private int maxHealth;

    [HideInInspector]
    public int health;   

    private void Start()
    {
        health = maxHealth;
        InitializeDamageController();
    }

    protected void InitializeDamageController()
    {
        damageController = gameObject.GetComponent<DamageController>();

        if (damageController != null)
        {
            SetDamageType();
        }
    }

    protected virtual void SetDamageType()
    {

    }
}
